resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "local_file" "k8_ssh_key" {
    filename = "k8_ssh_key.pem"
    file_permission = "400"
    content  = tls_private_key.ssh.private_key_pem
}

resource "aws_key_pair" "k8_ssh_2" {
  key_name   = "k8_ssh_2"
  public_key = tls_private_key.ssh.public_key_openssh
}